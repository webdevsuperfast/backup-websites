## Backup WordPress Sites without Plugin
### Method 1
* Make sure SSH is enabled on client server.
* Run `ssh username@websitename.com`
* Run `mysqldump -u dbusername -p dbname > backup_dbname.sql`
* Once inside run `rsync -avz public_html/ testservername@testserver.com:public_html/testfolder`
* Log in to your test server using SSH.
* Navigate to the test site folder with `cd public_html/subdomain-folder`.
* Run `find . -type f -exec chmod 644 {} \;` for files.
* Run `find . -type d -exec chmod 755 {} \;` for directories.    
* Wait until finished.
* Update URL's. Check out WordPress documentation on this(https://codex.wordpress.org/Changing_The_Site_URL).
 

### Method 2
* Login to your client's webhost(cPanel preferred).
* Compressed `wp-content` folder.
* Backup database using `phpmyadmin` or `WP DB Migrate` plugin and download.
* Login to your test server using ssh.
* Go into your preferred folder with `cd public_html/subdomain-folder`.
* Download the compressed wp-content(wp-content.zip) file using wget e.g. `wget http://example.net/wp-content.zip`.
* Login to your test server webhost(cPanel preferred).
* Navigate to your test site folder with `cd public_html/subdomain-folder`.
* Install WordPress manually.
* After installation remove wp-content folder and uncompress the `wp-content.zip` downloaded earlier. If you want to use SSH to uncompress zipped files you can do `unzip wp-content` with or without the file extension.
* Log in to your test server using SSH.
* Navigate to the test site folder.
* Run `find . -type f -exec chmod 644 {} \;` for files.
* Run `find . -type d -exec chmod 755 {} \;` for directories.
* Drop database content from original installation and import the database backup from client site.
* Update URL's. Check out WordPress documentation on this(https://codex.wordpress.org/Changing_The_Site_URL).

### Method 3a
* Log into your client's webhost(cPanel preferred).
* Create database backup via `mysqldump` or any other tool.
* Create exclude.txt file. With SSH you can use vim `vi exclude.txt`.
* Input the following:
    ```
    .tar
    .gz
    .tar.gz
    .zip
    .7z
    .rar```
    ** You can also add directories/specific files into it.
* Type `tar -zcvf public_html/backup_websitename.tar.gz -X exclude.txt public_html`.
* Now log into your test sever via SSH.
* Navigate to the test site folder
* Use wget/curl to pull the backup file from the client's site e.g. `wget http://example.com/backup_websitename.tar.gz`.
* Wait until finished downloading.
* Type `tar -zxvf backup_websitename.tar.gz` to extract.
* Run `find . -type f -exec chmod -exec 644 {} \;` for files.
* Run `find . -type d -exec chmod -exec 755 {} \;` for directories.
* Import database.
* Update URL's. Check out WordPress documentation on this(https://codex.wordpress.org/Changing_The_Site_URL).

### Method 3b
* Log into your client's webhost(cPanel preferred).
* Create database backup via `mysqldump` or via phpmyadmin.
* Browse into the `public_html` folder and compress all content
* Optional: rename the created compressed file to `backup.zip` to remember it easily.
* Now log into your test sever via SSH.
* Navigate to the test site folder
* Use wget/curl to pull the backup file from the client's site e.g. `wget http://example.com/backup.zip`.
* Wait until finished downloading.
* Type `unzip backup` to extract. ** Note `backup` is the filename of the zipped file.
* Run `find . -type f -exec chmod 644 {} \;` for files.
* Run `find . -type d -exec chmod 755 {} \;` for directories.
* Import database.
* Update URLs.

### Method 4
* Login to your test server using SSH.
* Go to your test site folder `e.g. cd public_html/backup`
* Install WordPress using wp-cli or manually.
* Rename `wp-content` folder `e.g. mv wp-content wp-content.old`
* Use wget and ftp to access your client's live server `e.g. wget -r -nH --cut-dirs=2 --ask-password ftp://username@ftp.example.com/wp-content/`
* Wait until finish.
* Run `find . -type f -exec chmod 644 {} \;` for files.
* Run `find . -type d -exec chmod 755 {} \;` for directories.
* Export and import database.
* Update URL's. Check out WordPress documentation on this(https://codex.wordpress.org/Changing_The_Site_URL).